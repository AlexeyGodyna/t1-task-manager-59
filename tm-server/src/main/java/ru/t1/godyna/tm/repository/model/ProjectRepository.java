package ru.t1.godyna.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.api.repository.model.IProjectRepository;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public void clearAll() {
        @Nullable final List<Project> projects = findAll();
        if (projects == null) return;
        for (@NotNull Project project : projects) {
            entityManager.remove(project);
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId.isEmpty()) return;
        @Nullable final List<Project> projects = findAllUserId(userId);
        if (projects == null) return;
        for (Project project : projects) {
            entityManager.remove(project);
        }
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Project m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty()) return false;
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Project m WHERE m.id = :id and m.user.id = :userId";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        @NotNull final String jpql = "SELECT m FROM Project m";
        return entityManager.createQuery(jpql, Project.class).getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final Comparator comparator) {
        @NotNull final String jpql = "SELECT m FROM Project m ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, Project.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final Sort sort) {
        @NotNull final String jpql = "SELECT m FROM Project m ORDER BY m." + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, Project.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAllUserId(@Nullable final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }


    @Nullable
    @Override
    public List<Project> findAllUserId(@NotNull final String userId, @NotNull final Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId ORDER BY m." + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAllUserId(@NotNull final String userId, @NotNull final Comparator comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String id) {
        return entityManager.find(Project.class, id);
    }

    @Nullable
    @Override
    public Project findOneByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.id = :id AND m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Project m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getSizeUserId(@Nullable final String userId) {
        if (userId.isEmpty()) return 0;
        @NotNull final String jpql = "SELECT COUNT(m) FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final String jpql = "DELETE FROM Project m WHERE m.id = :id";
        entityManager.createQuery(jpql)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeByIdUserId(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String jpql = "DELETE FROM Project m WHERE m.id = :id AND m.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
