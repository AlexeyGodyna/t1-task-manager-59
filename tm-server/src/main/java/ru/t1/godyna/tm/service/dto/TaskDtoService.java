package ru.t1.godyna.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.godyna.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.godyna.tm.api.service.dto.ITaskDtoService;
import ru.t1.godyna.tm.comparator.NameComparator;
import ru.t1.godyna.tm.dto.model.TaskDTO;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.TaskNotFoundException;
import ru.t1.godyna.tm.exception.field.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public TaskDTO add(@Nullable TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        model.setUserId(userId);
        taskRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        for (@NotNull TaskDTO task : models) {
                taskRepository.add(task);
            }
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.clearAll();
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.clear(userId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        return add(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.existsByIdUserId(userId, id);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findAllUserId(userId);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return taskRepository.findAllUserId(userId, NameComparator.INSTANCE);
        return taskRepository.findAllUserId(userId, sort);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable Comparator<TaskDTO> comparator) {
        if (comparator == null) return taskRepository.findAll(NameComparator.INSTANCE);
        return taskRepository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator<TaskDTO> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return taskRepository.findAllUserId(userId, NameComparator.INSTANCE);
        return taskRepository.findAllUserId(userId, comparator);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneById(id);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneByIdUserId(userId, id);
    }

    @Override
    public long getSize() {
        return taskRepository.getSize();
    }

    @Override
    public long getSize(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.getSizeUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO remove(@Nullable TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO remove(@Nullable String userId, @Nullable TaskDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        taskRepository.removeByIdUserId(userId, model.getId());
        return model;
    }

    @Override
    @Transactional
    public void removeAll(@Nullable Collection<TaskDTO> collection) {
        if (collection == null) throw new TaskNotFoundException();
        for (@NotNull TaskDTO task : collection) {
            taskRepository.remove(task);
        }
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO task = findOneById(id);
        taskRepository.removeById(id);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO task = findOneById(id);
        taskRepository.removeById(id);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        clear();
        add(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO update(@NotNull TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.update(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.update(task);
        return task;
    }

}
