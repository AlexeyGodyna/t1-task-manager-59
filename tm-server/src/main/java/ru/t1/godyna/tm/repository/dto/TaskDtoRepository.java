package ru.t1.godyna.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.godyna.tm.dto.model.TaskDTO;
import ru.t1.godyna.tm.enumerated.Sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> implements ITaskDtoRepository {

    @Override
    public void clearAll() {
        @NotNull final String jpql = "DELETE FROM TaskDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DElETE FROM TaskDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM TaskDTO m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty()) return false;
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM TaskDTO m WHERE m.id = :id and m.userId = :userId";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m";
        return entityManager.createQuery(jpql, TaskDTO.class).getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@NotNull final Comparator comparator) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m ORDER BY m."
                + getSortType(comparator);
        return entityManager.createQuery(jpql, TaskDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@NotNull final Sort sort) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, TaskDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllUserId(@Nullable final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class).
                setParameter("userId", userId)
                .getResultList();
    }


    @Nullable
    @Override
    public List<TaskDTO> findAllUserId(@NotNull final String userId, @NotNull final Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllUserId(@NotNull final String userId, @NotNull final Comparator comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId ORDER BY m."
                + getSortType(comparator);
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId.isEmpty()) return Collections.emptyList();
        if (projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId and m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.id = :id AND m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM TaskDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getSizeUserId(@Nullable final String userId) {
        if (userId.isEmpty()) return 0;
        @NotNull final String jpql = "SELECT COUNT(m) FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.id = :id";
        entityManager.createQuery(jpql)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeByIdUserId(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.id = :id AND m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeTasksByProjectId(@NotNull final String projectId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.projectId = :projectId";
        entityManager.createQuery(jpql)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
