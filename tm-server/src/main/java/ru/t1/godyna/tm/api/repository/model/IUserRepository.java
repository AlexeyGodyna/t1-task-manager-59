package ru.t1.godyna.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.model.User;

import java.util.List;

public interface IUserRepository {

    void add(@NotNull User user);

    @Nullable
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    void remove(@NotNull User user);

    void update(@NotNull User user);

}
