package ru.t1.godyna.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.godyna.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserDtoRepository implements IUserDtoRepository {

    @NotNull
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void add(@NotNull UserDTO user) {
        entityManager.persist(user);
    }

    @Override
    public @Nullable List<UserDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDTO m";
        return entityManager.createQuery(jpql, UserDTO.class).getResultList();
    }

    @Override
    public @Nullable UserDTO findOneById(@Nullable String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Override
    public @Nullable UserDTO findByLogin(@Nullable String login) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable UserDTO findByEmail(@Nullable String email) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull UserDTO user) {
        entityManager.remove(user);
    }

    @Override
    public void update(@NotNull UserDTO user) {
        entityManager.merge(user);
    }

}
