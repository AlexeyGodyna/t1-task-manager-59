package ru.t1.godyna.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.godyna.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.godyna.tm.api.service.dto.IProjectDtoService;
import ru.t1.godyna.tm.dto.model.ProjectDTO;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.ProjectNotFoundException;
import ru.t1.godyna.tm.exception.entity.StatusEmptyException;
import ru.t1.godyna.tm.exception.field.DescriptionEmptyException;
import ru.t1.godyna.tm.exception.field.IdEmptyException;
import ru.t1.godyna.tm.exception.field.NameEmptyException;
import ru.t1.godyna.tm.exception.field.UserIdEmptyException;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public class ProjectDtoService implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@Nullable final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@Nullable final String userId, @Nullable final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        model.setUserId(userId);
        projectRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> add(@NotNull final Collection<ProjectDTO> models) {
        if (models == null) throw new ProjectNotFoundException();
        for (@NotNull ProjectDTO project : models) {
            projectRepository.add(project);
        }
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUserId(userId);
        return update(project);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.clearAll();
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        projectRepository.clear(userId);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.existsById(id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.existsByIdUserId(userId, id);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllUserId(userId);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final Comparator<ProjectDTO> comparator) {
        return projectRepository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Comparator<ProjectDTO> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllUserId(userId, comparator);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllUserId(userId, sort);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findOneById(id);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findOneByIdUserId(userId, id);
    }

    @Override
    public long getSize() {
        return projectRepository.getSize();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.getSizeUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO remove(@Nullable final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO remove(@Nullable final String userId, @Nullable final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.removeByIdUserId(userId, model.getId());
        return model;
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final Collection<ProjectDTO> collection) {
        if (collection == null) throw new ProjectNotFoundException();
        for (@NotNull ProjectDTO project : collection) {
            projectRepository.remove(project);
        }
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO project = projectRepository.findOneById(id);
        projectRepository.removeById(id);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO project = projectRepository.findOneByIdUserId(userId, id);
        projectRepository.removeByIdUserId(userId, id);
        return project;

    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> collections) {
        if (collections == null) throw new ProjectNotFoundException();
        clear();
        add(collections);
        return collections;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO update(@NotNull final ProjectDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.update(model);
        return model;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.update(project);
        return project;
    }

}
