package ru.t1.godyna.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.godyna.tm.comparator.CreatedComparator;
import ru.t1.godyna.tm.comparator.StatusComparator;
import ru.t1.godyna.tm.model.AbstractUserOwnedModel;
import ru.t1.godyna.tm.model.User;

import java.util.Comparator;

@NoArgsConstructor
public abstract class AbstractUserOwnedRepository <M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.merge(model);
    }

    public void removeUserId(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        if(existsByIdUserId(userId, model.getId())) entityManager.remove(model);
    }

}
