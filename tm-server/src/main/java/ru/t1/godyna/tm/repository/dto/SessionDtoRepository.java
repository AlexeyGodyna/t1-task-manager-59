package ru.t1.godyna.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.godyna.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDtoRepository implements ISessionDtoRepository {

    @NotNull
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void add(@NotNull final SessionDTO session) {
        entityManager.persist(session);
    }

    @Override
    public void update(@NotNull final SessionDTO session) {
        entityManager.merge(session);
    }

    @Override
    public void remove(@NotNull final SessionDTO session) {
        entityManager.remove(session);
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        if(userId.isEmpty()) return;
        @NotNull String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class).getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Nullable
    @Override
    public SessionDTO findOneByIdUserId(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.id = :id AND m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

}
