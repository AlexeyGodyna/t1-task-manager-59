//package ru.t1.godyna.tm.repository;
//
//import liquibase.Liquibase;
//import liquibase.exception.LiquibaseException;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.*;
//import ru.t1.godyna.tm.api.repository.dto.IProjectDtoRepository;
//import ru.t1.godyna.tm.api.repository.dto.ITaskDtoRepository;
//import ru.t1.godyna.tm.api.repository.dto.IUserDtoRepository;
//import ru.t1.godyna.tm.api.service.IConnectionService;
//import ru.t1.godyna.tm.api.service.IPropertyService;
//import ru.t1.godyna.tm.dto.model.TaskDTO;
//import ru.t1.godyna.tm.migration.AbstractSchemeTest;
//import ru.t1.godyna.tm.repository.dto.ProjectDtoRepository;
//import ru.t1.godyna.tm.repository.dto.TaskDtoRepository;
//import ru.t1.godyna.tm.repository.dto.UserDtoRepository;
//import ru.t1.godyna.tm.service.PropertyService;
//
//import javax.persistence.EntityManager;
//
//import static ru.t1.godyna.tm.constant.ProjectTestData.USER1_PROJECT1;
//import static ru.t1.godyna.tm.constant.ProjectTestData.USER2_PROJECT1;
//import static ru.t1.godyna.tm.constant.TaskTestData.*;
//import static ru.t1.godyna.tm.constant.UserTestData.*;
//
//public class TaskDtoRepositoryTest extends AbstractSchemeTest {
//
//    @Nullable
//    private static EntityManager entityManager;
//
//    @Nullable
//    private static IUserDtoRepository userRepository;
//
//    @Nullable
//    private static IProjectDtoRepository projectRepository;
//
//    @Nullable
//    private static ITaskDtoRepository taskRepository;
//
//    @BeforeClass
//    public static void init() throws LiquibaseException {
//        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
//        liquibase.dropAll();
//        liquibase.update("scheme");
//
//        @NotNull IPropertyService propertyService = new PropertyService();
//        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
//        entityManager = connectionService.getEntityManager();
//        userRepository = new UserDtoRepository(entityManager);
//        projectRepository = new ProjectDtoRepository(entityManager);
//        taskRepository = new TaskDtoRepository(entityManager);
//    }
//
//    @AfterClass
//    public static void connectionClose() {
//        userRepository.remove(USER1);
//        entityManager.close();
//    }
//
//    @Before
//    public void transactionStart() {
//        entityManager.getTransaction().begin();
//        if (userRepository.findOneById(USER1.getId()) == null) userRepository.add(USER1);
//        if (userRepository.findOneById(USER2.getId()) == null) userRepository.add(USER2);
//        if (userRepository.findOneById(ADMIN.getId()) == null) userRepository.add(ADMIN);
//        if (projectRepository.findOneById(USER1_PROJECT1.getId()) == null) projectRepository.add(USER1_PROJECT1);
//        if (projectRepository.findOneById(USER2_PROJECT1.getId()) == null) projectRepository.add(USER2_PROJECT1);
//    }
//
//    @After
//    public void transactionEnd() {
//        entityManager.getTransaction().rollback();
//    }
//
//    @Test
//    public void add() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1_TASK1.getId()).getId());
//    }
//
//    @Test
//    public void clear() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        for (@NotNull final TaskDTO task : USER1_TASK_LIST) {
//            taskRepository.add(task);
//        }
//        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.findAll().size());
//        taskRepository.clear(USER2.getId());
//        Assert.assertFalse(taskRepository.findAll().isEmpty());
//        taskRepository.clear(USER1.getId());
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER2_TASK1);
//        taskRepository.clear(USER1.getId());
//        Assert.assertFalse(taskRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void clearAll() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        for (@NotNull final TaskDTO task : USER1_TASK_LIST) {
//            taskRepository.add(task);
//        }
//        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.findAll().size());
//        taskRepository.clearAll();
//        Assert.assertEquals(0, taskRepository.findAll().size());
//    }
//
//    @Test
//    public void existsById() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        Assert.assertTrue(taskRepository.existsById(USER1_TASK1.getId()));
//        Assert.assertFalse(taskRepository.existsById(USER2_TASK1.getId()));
//    }
//
//    @Test
//    public void existsByIdUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        Assert.assertTrue(taskRepository.existsById(USER1_TASK1.getId()));
//        Assert.assertFalse(taskRepository.existsById(USER2_TASK1.getId()));
//    }
//
//    @Test
//    public void findAll() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        for (@NotNull TaskDTO task : USER1_TASK_LIST) {
//            taskRepository.add(task);
//        }
//        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.findAll().size());
//    }
//
//    @Test
//    public void findAllUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        for (@NotNull TaskDTO task : TASK_LIST) {
//            taskRepository.add(task);
//        }
//        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.findAllUserId(USER1.getId()).size());
//    }
//
//    @Test
//    public void findAllByProjectId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        for (@NotNull TaskDTO task: USER1_TASK_LIST) {
//            taskRepository.add(task);
//        }
//        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).size());
//    }
//
//    @Test
//    public void findOneById() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        taskRepository.add(USER2_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1_TASK1.getId()).getId());
//    }
//
//    @Test
//    public void findOneByIdUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneByIdUserId(USER1.getId(), USER1_TASK1.getId()).getId());
//    }
//
//    @Test
//    public void getSize() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        for (@NotNull TaskDTO task : TASK_LIST) {
//            taskRepository.add(task);
//        }
//        Assert.assertEquals(TASK_LIST.size(), taskRepository.getSize());
//    }
//
//    @Test
//    public void getSizeUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        for (@NotNull TaskDTO task : TASK_LIST) {
//            taskRepository.add(task);
//        }
//        Assert.assertEquals(USER1_TASK_LIST.size(), taskRepository.getSizeUserId(USER1.getId()));
//        Assert.assertEquals(USER2_TASK_LIST.size(), taskRepository.getSizeUserId(USER2.getId()));
//        Assert.assertEquals(ADMIN1_TASK_LIST.size(), taskRepository.getSizeUserId(ADMIN.getId()));
//    }
//
//    @Test
//    public void remove() {
//        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
//        taskRepository.add(USER1_TASK1);
//        Assert.assertNotNull(taskRepository.findOneById(USER1_TASK1.getId()));
//        taskRepository.remove(USER1_TASK1);
//        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
//    }
//
//    @Test
//    public void removeUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        for (@NotNull TaskDTO task : USER1_TASK_LIST) {
//            taskRepository.add(task);
//        }
//        taskRepository.clear(USER2.getId());
//        Assert.assertFalse(taskRepository.findAll().isEmpty());
//        taskRepository.clear(USER1.getId());
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER2_TASK1);
//        taskRepository.clear(USER1.getId());
//        Assert.assertFalse(taskRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void removeById() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        taskRepository.add(USER2_TASK1);
//        taskRepository.removeById(USER1_TASK1.getId());
//        entityManager.flush();
//        entityManager.clear();
//        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
//    }
//
//    @Test
//    public void removeByIdUserId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        taskRepository.add(USER2_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1_TASK1.getId()).getId());
//        Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findOneById(USER2_TASK1.getId()).getId());
//        taskRepository.removeByIdUserId(USER1.getId(), USER1_TASK1.getId());
//        entityManager.flush();
//        entityManager.clear();
//        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
//        Assert.assertEquals(1, taskRepository.findAll().size());
//    }
//
//    @Test
//    public void removeTasksByProjectId() {
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//        taskRepository.add(USER1_TASK1);
//        taskRepository.add(USER2_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1_TASK1.getId()).getId());
//        Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findOneById(USER2_TASK1.getId()).getId());
//        taskRepository.removeTasksByProjectId(USER1_PROJECT1.getId());
//        entityManager.flush();
//        entityManager.clear();
//        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
//        Assert.assertEquals(1, taskRepository.findAll().size());
//    }
//
//    @Test
//    public void update() {
//        @NotNull TaskDTO task = new TaskDTO("TEST", "TEST");
//        task.setUserId(USER1.getId());
//        taskRepository.add(task);
//        Assert.assertEquals(task.getName(), taskRepository.findOneById(task.getId()).getName());
//        task.setName("TEST_TEST");
//        taskRepository.update(task);
//        Assert.assertEquals(task.getName(), taskRepository.findOneById(task.getId()).getName());
//    }
//
//}
