package ru.t1.godyna.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserLogoutRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLogoutRequest(@Nullable final String token) {
        super(token);
    }

}
