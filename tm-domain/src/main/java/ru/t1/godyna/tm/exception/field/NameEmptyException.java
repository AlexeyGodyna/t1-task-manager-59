package ru.t1.godyna.tm.exception.field;

public final class NameEmptyException extends AbsractFieldException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}
