package ru.t1.godyna.tm.exception.entity;

public final class StatusEmptyException extends AbstractEntityNotFoundException {

    public StatusEmptyException() {
        super("Error! Status is empty...");
    }

}
