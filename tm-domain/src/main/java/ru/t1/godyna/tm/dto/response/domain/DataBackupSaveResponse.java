package ru.t1.godyna.tm.dto.response.domain;

import lombok.NoArgsConstructor;
import ru.t1.godyna.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class DataBackupSaveResponse extends AbstractResponse {
}
