package ru.t1.godyna.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.godyna.tm.api.endpoint.IAuthEndpoint;
import ru.t1.godyna.tm.api.endpoint.IProjectEndpoint;
import ru.t1.godyna.tm.dto.model.ProjectDTO;
import ru.t1.godyna.tm.dto.request.project.*;
import ru.t1.godyna.tm.dto.request.user.UserLoginRequest;
import ru.t1.godyna.tm.dto.response.project.*;
import ru.t1.godyna.tm.dto.response.user.UserLoginResponse;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.marker.ISoapCategory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Category(ISoapCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String userToken;

    @NotNull
    private final Random random = new Random();

    @Before
    public void init() {
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        userToken = loginResponse.getToken();
    }

    @Test
    public void projectCreate() {
        @NotNull final String testProjectName = "TestProject";
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest(null, null, null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(
                new ProjectCreateRequest(UUID.randomUUID().toString(), "projectName", "description"))
        );
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "description")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
    }

    @Test
    public void projectRemoveById() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(
                new ProjectRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(
                new ProjectRemoveByIdRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(
                new ProjectRemoveByIdRequest(UUID.randomUUID().toString(), "Id"))
        );
        final int projectsLength = 10;
        @Nullable List<ProjectDTO> projects = new ArrayList<>();
        for (int i = 0; i < projectsLength; i++) {
            @NotNull final String testProjectName = "TestProject_" + i;
            @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                    new ProjectCreateRequest(userToken, testProjectName, "description")
            );
            projects.add(createResponse.getProject());
        }
        final int randomIndex = random.nextInt(projectsLength - 1);
        @Nullable final ProjectDTO project = projects.get(randomIndex);
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(userToken, project.getId()));
        Assert.assertNull(projectEndpoint.showProjectById(new ProjectShowByIdRequest(userToken, project.getId())).getProject());
    }

    @Test
    public void projectUpdateById() {
        @NotNull final String testProjectName = "TestProject";
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(null, null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(UUID.randomUUID().toString(), null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(userToken, "Id", null, null)
        ));
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "description")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        @Nullable ProjectUpdateByIdResponse updateResponse = projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(userToken, createResponse.getProject().getId(), "testName", "testDescription")
        );
        Assert.assertNotNull(updateResponse);
        Assert.assertNotNull(updateResponse.getProject());
        Assert.assertNotEquals(createResponse.getProject().getName(), updateResponse.getProject().getName());
        Assert.assertNotEquals(createResponse.getProject().getDescription(), updateResponse.getProject().getDescription());
        Assert.assertEquals(createResponse.getProject().getId(), updateResponse.getProject().getId());
    }

    @Test
    public void projectList() {
        @NotNull final String testProjectName = "TestProject";
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest("", null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(
                new ProjectListRequest(UUID.randomUUID().toString(), null))
        );
        @Nullable ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "description")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
        @Nullable ProjectListResponse response = projectEndpoint.listProject(
                new ProjectListRequest(userToken, Sort.BY_NAME)
        );
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProjects());
    }

    @Test
    public void projectClear() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(
                new ProjectClearRequest())
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(
                new ProjectClearRequest(null))
        );
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(
                new ProjectClearRequest(UUID.randomUUID().toString()))
        );
        final int projectsLength = 10;
        for (int i = 0; i < projectsLength; i++) {
            @NotNull final String testProjectName = "TestProject_" + i;
            projectEndpoint.createProject(new ProjectCreateRequest(userToken, testProjectName, "description"));
        }
        @Nullable ProjectClearResponse response = projectEndpoint.clearProject(
                new ProjectClearRequest(userToken)
        );
        @Nullable ProjectListResponse projectList = projectEndpoint.listProject(
                new ProjectListRequest(userToken, Sort.BY_NAME)
        );
        Assert.assertNotNull(response);
    }

    @Test
    public void projectChangeStatusById() {
        @NotNull final String testProjectName = "TestProject";
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest())
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(null, null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(UUID.randomUUID().toString(), null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(userToken, null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(userToken, testProjectName, null))
        );
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(userToken, UUID.randomUUID().toString(), Status.IN_PROGRESS))
        );
        @Nullable final ProjectCreateResponse projectCreateResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "description")
        );
        Assert.assertNotNull(projectCreateResponse);
        Assert.assertNotNull(projectCreateResponse.getProject());
        @Nullable final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(userToken, projectCreateResponse.getProject().getId(), Status.IN_PROGRESS)
        );
        Assert.assertNotNull(response);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

}
